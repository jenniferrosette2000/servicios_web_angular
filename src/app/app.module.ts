import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { JenniComponent } from './components/jenni/jenni.component';
import { MonseComponent } from './components/monse/monse.component';
import { ComponenteGustosComponent } from './components/componenteMonseGustos/componente-gustos.component';
import { ChristopherComponent } from './components/christopher/christopher.component';
import { ComponenteChristopherGustosComponent } from './components/componente-christopher-gustos/componente-christopher-gustos.component';
import { HolasoyJenniComponent } from './components/holasoy-jenni/holasoy-jenni.component';
import { FernandoComponent } from './components/fernando/fernando.component';
import { ComponenteFernandoComponent } from './components/componente-fernando/componente-fernando.component';
import { JesusComponent } from './components/jesus/jesus.component';
import { ComponenteJesusGustosComponent } from './components/componente-jesus-gustos/componente-jesus-gustos.component';
import { JnthnComponent } from './components/jnthn/jnthn.component';
import { MarielaComponent } from './components/mariela/mariela.component';
import { MarielagustosComponent } from './components/marielagustos/marielagustos.component';

@NgModule({
  declarations: [
    AppComponent,
    JenniComponent,
    MonseComponent,
    ChristopherComponent,
    ComponenteGustosComponent,
    ComponenteChristopherGustosComponent,
    HolasoyJenniComponent,
    FernandoComponent,
    ComponenteFernandoComponent,
    JesusComponent,
    ComponenteJesusGustosComponent,
    JnthnComponent,
    MarielaComponent,
    MarielagustosComponent,

  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
