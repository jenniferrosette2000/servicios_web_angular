import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarielaComponent } from './mariela.component';

describe('MarielaComponent', () => {
  let component: MarielaComponent;
  let fixture: ComponentFixture<MarielaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarielaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MarielaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
