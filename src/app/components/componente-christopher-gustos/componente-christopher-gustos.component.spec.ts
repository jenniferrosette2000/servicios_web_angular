import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponenteChristopherGustosComponent } from './componente-christopher-gustos.component';

describe('ComponenteChristopherGustosComponent', () => {
  let component: ComponenteChristopherGustosComponent;
  let fixture: ComponentFixture<ComponenteChristopherGustosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComponenteChristopherGustosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ComponenteChristopherGustosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
