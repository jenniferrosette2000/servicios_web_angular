import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HolasoyJenniComponent } from './holasoy-jenni.component';

describe('HolasoyJenniComponent', () => {
  let component: HolasoyJenniComponent;
  let fixture: ComponentFixture<HolasoyJenniComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HolasoyJenniComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HolasoyJenniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
