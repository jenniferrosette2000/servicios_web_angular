import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChristopherComponent } from './christopher.component';

describe('ChristopherComponent', () => {
  let component: ChristopherComponent;
  let fixture: ComponentFixture<ChristopherComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChristopherComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ChristopherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
