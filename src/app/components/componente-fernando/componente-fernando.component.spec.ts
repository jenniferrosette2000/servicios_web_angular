import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponenteFernandoComponent } from './componente-fernando.component';

describe('ComponenteFernandoComponent', () => {
  let component: ComponenteFernandoComponent;
  let fixture: ComponentFixture<ComponenteFernandoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComponenteFernandoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ComponenteFernandoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
