import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JenniComponent } from './jenni.component';

describe('JenniComponent', () => {
  let component: JenniComponent;
  let fixture: ComponentFixture<JenniComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JenniComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(JenniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
