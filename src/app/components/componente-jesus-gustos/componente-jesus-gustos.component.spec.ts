import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponenteJesusGustosComponent } from './componente-jesus-gustos.component';

describe('ComponenteJesusGustosComponent', () => {
  let component: ComponenteJesusGustosComponent;
  let fixture: ComponentFixture<ComponenteJesusGustosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComponenteJesusGustosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ComponenteJesusGustosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
