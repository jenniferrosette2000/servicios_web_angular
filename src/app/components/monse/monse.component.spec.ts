import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonseComponent } from './monse.component';

describe('MonseComponent', () => {
  let component: MonseComponent;
  let fixture: ComponentFixture<MonseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MonseComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MonseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
