import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarielagustosComponent } from './marielagustos.component';

describe('MarielagustosComponent', () => {
  let component: MarielagustosComponent;
  let fixture: ComponentFixture<MarielagustosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarielagustosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MarielagustosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
