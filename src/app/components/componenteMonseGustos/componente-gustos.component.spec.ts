import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponenteGustosComponent } from './componente-gustos.component';

describe('ComponenteGustosComponent', () => {
  let component: ComponenteGustosComponent;
  let fixture: ComponentFixture<ComponenteGustosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComponenteGustosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ComponenteGustosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
