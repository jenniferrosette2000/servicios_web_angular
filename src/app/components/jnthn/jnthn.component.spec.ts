import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JnthnComponent } from './jnthn.component';

describe('JnthnComponent', () => {
  let component: JnthnComponent;
  let fixture: ComponentFixture<JnthnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JnthnComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(JnthnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
